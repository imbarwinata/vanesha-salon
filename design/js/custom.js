if($(window).width() < 758) {
    $("#logo-primary").hide();
	$(".navbar-brand").show();
	$("li.social.pull-right").hide();
}
else{
	$("#logo-primary").show();
	$(".navbar-brand").hide();
	$("li.social.pull-right").show();
}

$(window).resize(function() {
	if($(window).width() < 758) {
    	$("#logo-primary").hide();
		$(".navbar-brand").show();
		$("li.social.pull-right").hide();
	}
	else{
		$("#logo-primary").show();
		$(".navbar-brand").hide();
		$("li.social.pull-right").show();
 	}	
});

	
$(window).scroll(function (event) {
  var scroll = $(window).scrollTop();
  if(scroll >= 400){
    /*$("#navbar").hide(100);*/
  }
  else if(scroll <= 400){
    /*$("#navbar").show(100);*/
  }
});

function toggleSearch(){
	$(".form-search").toggle(200);
}// JavaScript Document