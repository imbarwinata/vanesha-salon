<?php
class generate_model{
	function generateID($code = "", $table = "", $id = ""){
		$query = $db->query("SELECT * FROM ".$table." ORDER BY ".$id."","row");
	}

	function generate_custom_id($code = "", $date_format = "", $table = "", $id_label = "", $limit_number_length = 1){
		include_once('query_model.php');
		$db = new query_model();

		$length_code = strlen($code);

		$date_time = new DateTime();
		$today = $date_time->format($date_format);
		$length_today = strlen($today);

		$prefix_length = $length_code+$length_today;

		$prefix_code_date = $code.$today;

		$result = $db->query("SELECT * FROM $table WHERE LEFT($id_label, $prefix_length) = '$prefix_code_date' ORDER BY CAST(SUBSTRING($id_label, $prefix_length) AS unsigned) DESC","row");
		if(count($result) <= 0){
			return $code.$today.$this->get_number_by_length($limit_number_length);
		} else {
			$current_id = $result[$id_label];
			$number = substr($current_id, $prefix_length);
			$number = (int)$number;
			$number++;
			$number = strval($number);
			$length_number_after_plus = strlen($number);
			if($length_number_after_plus < $limit_number_length){
				$remaining = $limit_number_length - $length_number_after_plus;
				$number = $this->get_number_by_length($remaining, false).$number;
			}
			return $code.$today.$number;
		}
	}

	function get_number_by_length($limit_number_length = 0, $flag = true){
		$result = "";
		if($flag == true){
			for ($i=1; $i < $limit_number_length; $i++) { 
				$result .= "0";
			}

			$result .= "1";
		} else {
			for ($i=0; $i < $limit_number_length; $i++) { 
				$result .= "0";
			}
		}
		return $result;
	}

	function convertToUrlCompatible($string){
        $string = strtolower($string);
        $string = strip_tags($string);
        $string = stripslashes($string);
        $string = html_entity_decode($string);

        $string = str_replace('\'', "", $string);

        $match = '/[^a-z0-9]+/';
        $replace = '-';
        $string = preg_replace($match, $replace, $string);

        $string = trim($string, '-');

        return $string;
    }

	function checkPermalink($table_name, $permalink_field, $input_string, $field_id = "", $table_id = ""){
        $permalink = $this->convertToUrlCompatible($input_string);
        if($table_id != "" && $field_id != ""){
            $check = $this->query_model->getQuery('true', '*', $table_name, array($permalink_field => $permalink, $field_id => $table_id));
            if($check == true){
                return $permalink;
            } else {
                $check = $this->query_model->getQuery('true', '*', $table_name, array($permalink_field => $permalink, $field_id." != " => $table_id));
                if($check == true){
                    $flag = 0;
                    $number = 1;
                    $temp_permalink = "";
                    do{
                        $temp_permalink = $permalink.$number;
                        $check = $this->query_model->getQuery('true', '*', $table_name, array($permalink_field => $temp_permalink, $field_id." != " => $table_id));
                        if($check == false){
                            $flag = 1;
                        } else {
                            $number++;
                        }
                    }while($flag == 0);
                    return $temp_permalink;
                } else {
                    return $permalink;
                }
            }
        } else {
            $check = $this->query_model->getQuery('true', '*', $table_name, array($permalink_field => $permalink));
            if($check == true){
                $flag = 0;
                $number = 1;
                $temp_permalink = "";
                do{
                    $temp_permalink = $permalink.$number;
                    $check = $this->query_model->getQuery('true', '*', $table_name, array($permalink_field => $temp_permalink));
                    if($check == false){
                        $flag = 1;
                    } else {
                        $number++;
                    }
                }while($flag == 0);
                return $temp_permalink;
            } else {
                return $permalink;
            }
        }
    }
}
?>