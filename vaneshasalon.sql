-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2017 at 03:13 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaneshasalon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AdminID` int(11) NOT NULL,
  `AdminUserName` varchar(20) NOT NULL,
  `AdminPassword` varchar(50) NOT NULL,
  `AdminName` varchar(50) NOT NULL,
  `AdminEmail` varchar(50) NOT NULL,
  `AdminEntryName` varchar(50) NOT NULL,
  `AdminEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AdminID`, `AdminUserName`, `AdminPassword`, `AdminName`, `AdminEmail`, `AdminEntryName`, `AdminEntryDate`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'administrator@gmail.com', '-', '2017-02-15 17:16:07'),
(2, 'imbarwinata', 'adec79fd87d04960046628531c4a56b714d160d8', 'Imbar Winata', 'imbarwinata@gmail.com', 'admin', '2017-02-15 18:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `ContentID` varchar(20) NOT NULL,
  `ContentLabel` varchar(30) NOT NULL,
  `ContentTitle` varchar(30) NOT NULL,
  `ContentSubTitle` varchar(50) NOT NULL,
  `ContentDescription` text NOT NULL,
  `ContentLink` text NOT NULL,
  `ContentImage` text NOT NULL,
  `ContentDate` date NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`ContentID`, `ContentLabel`, `ContentTitle`, `ContentSubTitle`, `ContentDescription`, `ContentLink`, `ContentImage`, `ContentDate`, `AdminID`) VALUES
('C170322019', 'Home Banner', 'Mau tau tips kecantikan ?', 'Praesent sapien massa, convallis a pellentesque ne', '', 'https://www.google.co.id/', 'slide2.jpg', '2017-03-22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contentpoint`
--

CREATE TABLE `contentpoint` (
  `ContentPointID` varchar(20) NOT NULL,
  `ContentPointLabel` varchar(30) NOT NULL,
  `ContentPointTitle` varchar(50) NOT NULL,
  `ContentPointSubDescription` text NOT NULL,
  `ContentPointDescription` longtext NOT NULL,
  `ContentPointImage` text NOT NULL,
  `ContentPointDate` datetime NOT NULL,
  `ContentPointIcon` text NOT NULL,
  `ContentPointOrder` int(3) NOT NULL,
  `ContentPointShow` int(1) NOT NULL,
  `ContentPointPermalink` varchar(60) NOT NULL,
  `ContentPointView` int(11) NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contentpoint`
--

INSERT INTO `contentpoint` (`ContentPointID`, `ContentPointLabel`, `ContentPointTitle`, `ContentPointSubDescription`, `ContentPointDescription`, `ContentPointImage`, `ContentPointDate`, `ContentPointIcon`, `ContentPointOrder`, `ContentPointShow`, `ContentPointPermalink`, `ContentPointView`, `AdminID`) VALUES
('A17032200001', 'Article', 'Lorem Ipsum', 'Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.', '<p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>\r\n\r\n<p>Nulla porttitor accumsan tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Sed porttitor lectus nibh. Proin eget tortor risus.</p>', '16.jpg', '2017-03-22 00:00:00', '', 0, 0, 'Lorem-Ipsum-A17032200001', 0, 1),
('A17032200002', 'About', 'History', '', '<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh.&nbsp;<br />\r\n<br />\r\nCras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Cras ultricies ligula sed magna dictum porta.</p>', 'Fotolia_63073892_Subscription_Monthly_M1.jpeg', '2017-03-22 19:53:20', '', 0, 1, 'History-A17032200002', 0, 1),
('AB17032200001', 'About', 'Vision and Mission', '', '<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.&nbsp;<br />\r\n<br />\r\nDonec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque.</p>', 'beauty-salon-albury-1.jpg', '2017-03-22 19:49:36', '', 0, 1, 'Vision-and-MissionAB17032200001', 0, 1),
('AB17032200002', 'About', 'Objectives', '', '<p>Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.<br />\r\n<br />\r\nQuisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt.</p>', '2-Aman-Luxury-Spa-Mayfair.jpg', '2017-03-22 19:50:40', '', 0, 1, 'ObjectivesAB17032200002', 0, 1),
('FAQ17032200001', 'FAQ', 'Section 1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.</p>', '', '2017-03-22 20:13:17', '', 0, 1, 'Section-1FAQ17032200001', 0, 1),
('FAQ17032200002', 'FAQ', 'Section 2', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.</p>', '', '2017-03-22 20:13:27', '', 0, 1, 'Section-2FAQ17032200002', 0, 1),
('FAQ17032200003', 'FAQ', 'Section 3', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.</p>', '', '2017-03-22 20:13:44', '', 0, 1, 'Section-3FAQ17032200003', 0, 1),
('SML17032200002', 'Social Media Link', 'Google Plus', 'https://www.google.co.id/', '', '', '2017-03-22 21:04:35', 'fa-google-plus', 0, 1, '', 0, 1),
('SML17032200003', 'Social Media Link', 'Twitter', 'https://www.twitter.com/', '', '', '2017-03-22 21:10:05', 'fa-twitter', 0, 1, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `LocationID` varchar(20) NOT NULL,
  `LocationTitle` varchar(30) NOT NULL,
  `LocationKodePos` varchar(5) NOT NULL,
  `LocationFare` int(11) NOT NULL,
  `LocationShow` enum('0','1') NOT NULL,
  `LocationDate` datetime NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`LocationID`, `LocationTitle`, `LocationKodePos`, `LocationFare`, `LocationShow`, `LocationDate`, `AdminID`) VALUES
('L170215001', 'Cibinong', '16911', 8000, '1', '2017-02-15 13:53:56', 0),
('L170215002', 'Tamansari', '16610', 0, '1', '2017-02-15 15:15:39', 0),
('L170215003', 'Babakan Madang', '16810', 10000, '1', '2017-02-15 15:16:00', 0),
('L170215004', 'Cikoan', '16810', 0, '1', '2017-02-15 15:55:14', 0),
('L170215005', 'Ciomas', '16610', 0, '1', '2017-02-15 15:55:35', 0),
('L170215006', 'Gunung Sindur', '16340', 0, '1', '2017-02-15 15:55:55', 0),
('L170215007', 'Leuwisadeng', '16640', 0, '1', '2017-02-15 15:56:36', 0),
('L170215008', 'Ranca Bungur', '16310', 0, '1', '2017-02-15 15:57:03', 0),
('L170215009', 'Pamijahan', '16810', 0, '1', '2017-02-15 15:57:15', 0),
('L170215010', 'Bojonggede', '16920', 12000, '1', '2017-02-15 15:57:41', 0),
('L170215011', 'Cibungbulang', '16630', 5000, '1', '2017-03-22 19:00:48', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`ContentID`);

--
-- Indexes for table `contentpoint`
--
ALTER TABLE `contentpoint`
  ADD PRIMARY KEY (`ContentPointID`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`LocationID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AdminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
