<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="#">Home</a></li>
				<li><a href="#" class="current">Privacy Policy</a></li>
			</ol>
		</div>
	</div>
	
<!--	<section class="services-banner" style="background-image: url(design/img/castadiva-spa-wellness-01.jpg) url(design/img/layer-services.png);">
		<div class="services-banner-image" style="background-position:0px 0px; background-image: url(design/img/castadiva-spa-wellness-01.jpg);"></div>
	</section>-->
	
	<div id="banner2" style="background-position:0px 0px; background-image:url(design/img/castadiva-spa-wellness-01.jpg)">
		<h2><p style="text-align: center; padding-top: 6vw; color: rgba(255,255,255,1.00); font-family: 'Honeymoon-Up'; font-size: 3.3vw; font-weight: bold">Article</p></h2>
	</div>
	<section class="article-content">
		<div class="article-list">
			<div class="article-list-left" style="background-image: url(design/img/beauty-salon-albury-1.jpg)"></div>
			<div class="article-list-right">
				<h3>Tips Kecantikan</h3>
				<div>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.

					<br>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.
				</div>
				<a class="btn btn-flat btn-default pull-right btn-grap" href="?link=article_detail">Baca Selengkapnya</a>
			</div>
		</div>
		<div class="article-list">
			<div class="article-list-left" style="background-image: url(design/img/Fotolia_63073892_Subscription_Monthly_M1.jpeg);"></div>
			<div class="article-list-right">
				<h3>Tips Perawatan</h3>
				<div>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.

					<br>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.
				</div>
				<a class="btn btn-flat btn-default pull-right btn-grap" href="?link=article_detail">Baca Selengkapnya</a>
			</div>
		</div>
		<div class="article-list">
			<div class="article-list-left" style="background-image: url(design/img/Tips-Dan-Langkah-Mudah-Creambath-Ala-Salon-Di-Rumah-.jpg);"></div>
			<div class="article-list-right">
				<h3>Tips Kecantikan</h3>
				<div>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.

					<br>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.
				</div>
				<a class="btn btn-flat btn-default pull-right btn-grap" href="?link=article_detail">Baca Selengkapnya</a>
			</div>
		</div>
	</section>
	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" /><script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>