<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="#" class="current">Daftar</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Daftar</h4>
								<div class="account-form">
									<form method="post">
										<div class="form-group">
											<label for="email">Email *</label>
											<input type="email" class="form-control" name="email" style="border-radius: 0px;">
										</div>
										<div class="form-group">
											<label for="email">Password *</label>
											<input type="password" class="form-control" name="password" style="border-radius: 0px;">
										</div>
										<div class="form-group">
											<label for="email">Konfirmasi Password *</label>
											<input type="password" class="form-control" name="konfir_password" style="border-radius: 0px;">
										</div>
										<br>
										<div class="form-group">
											<label for="email">Nama *</label>
											<input type="text" class="form-control" name="nama" style="border-radius: 0px;">
										</div>
										<div class="form-group">
											<label for="email">Nomor Telepon *</label>
											<input type="number" class="form-control" name="telp" style="border-radius: 0px;">
										</div>
										<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
											Masuk
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>