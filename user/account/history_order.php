<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Akun</a></li>
				<li><a href="#" class="current">Daftar Pesanan</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-8">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Daftar Pesanan</h4>
								<div class="account-form">
									<div class="table-responsive">
										<table class="table table">
											<tr>
												<th>OrderID</th>
												<th>Date</th>
												<th>Status</th>
												<th>Total</th>
												<th></th>
											</tr>
											<tr>
												<td>OV2017030300001</td>
												<td>21 March 2017</td>
												<td>Pending</td>
												<td>Rp. 300.000,00</td>
												<td>
													<a href="?link=history_order_detail&id=OV2017030300001"><i class="fa fa-search" style="color: #A3838F;"></i></a> &nbsp; | &nbsp; 
													<a href="?link=confirm_order&id=OV2017030300001" class="label label-primary" style="padding: 5px 10px; background: #A3838F;">
														Konfirmasi Pesanan
													</a>
												</td>
											</tr>
											<tr>
												<td>OV2017030100001</td>
												<td>3 March 2017</td>
												<td>Selesai</td>
												<td>Rp. 170.000,00</td>
												<td>
													<a href="?link=history_order_detail&id=OV2017030300001"><i class="fa fa-search" style="color: #A3838F;"></i></a>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
							<!-- HTML -->
							<div id="account-id2">
								<h3 class="account-title" style="margin-bottom: 20px; margin-top: 15px;">Akun</h3>
								<div class="account">
									<ul>
										<li>
											<a href="?link=my_account"><i class="fa fa-user"></i> &nbsp; Akun Saya</a>
										</li>
										<li>
											<a href="?link=change_password"><i class="fa fa-lock"></i> &nbsp; Ganti Kata Sandi</a>
										</li>
										<li>
											<a href="#" class="current"><i class="fa fa-book"></i> &nbsp; Daftar Pesanan</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>