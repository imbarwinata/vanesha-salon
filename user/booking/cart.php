<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Pesanan</a></li>
				<li><a href="#" class="current">Daftar Keranjang Pesanan</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Daftar Keranjang Pesanan</h4>
								<div class="account-form">
									<div class="table-responsive">
										<table class="table table">
											<tr>
												<th></th>
												<th>Layanan</th>
												<th>Total Pesan</th>
												<th>Harga Satuan</th>
												<th>Subtotal</th>
											</tr>
											<tr>
												<td>
													<a onClick="if(confirm('Yakin akan hapus ?')){ window.location=('?link=hapus_pesanan&id=OV2017030300001') }" class="btn btn-danger"><i class="fa fa-trash" style="color:rgba(251,251,251,1.00);"></i></a>
												</td>
												<td>Sugar Wax</td>
												<td>1</td>
												<td>Rp. 150.000,00</td>
												<td>Rp. 150.000,00</td>
											</tr>
											<tr>
												<td>
													<a onClick="if(confirm('Yakin akan hapus ?')){ window.location=('?link=hapus_pesanan&id=OV2017030300001') }" class="btn btn-danger"><i class="fa fa-trash" style="color: rgba(251,251,251,1.00);"></i></a>
												</td>
												<td>Hard Wax</td>
												<td>2</td>
												<td>Rp. 160.000,00</td>
												<td>Rp. 320.000,00</td>
											</tr>
											<tr>
												<td colspan="5">
													<a href="href=?link=index" class="btn btn-primary btn-white-purple">Lanjutkan Memesan</a>
												</td>
											</tr>
										</table>
										<div class="col-lg-4 pull-right checkout">
											<table class="table table-checkout">
												<tr>
													<td>Subtotal</td>
													<td>:</td>
													<td>Rp. 470.000,00</td>
												</tr>
												<tr>
													<td>Ongkos</td>
													<td>:</td>
													<td>Rp. 10.000,00</td>
												</tr>
												<tr>
													<td>Total</td>
													<td>:</td>
													<td>Rp. 480.000,00</td>
												</tr>
												<tr>
													<td colspan="2"></td>
													<td><a href="" class="btn btn-danger btn-block">Checkout</a></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>