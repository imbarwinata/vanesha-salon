<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<section class="banner" id="image" style="background-image: url(design/img/background-banner2.jpg);">
		<div id="banner2"></div>
		<section class="banner-content">
			<h2>Mau tau tips kecantikan ?</h2>
			<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<button>Kunjungi</button>
		</section>
	</section>
	
	<section class="home-service">
		<h2 class="text-center">Top services</h2>
		<div class="space-15"></div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Cream bath and hair dry</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.

				<br><br>Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque. </p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url('bahan/img/Fotolia_63073892_Subscription_Monthly_M1.jpeg');"></div>
		</div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Facial by glowy</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.

				<br><br>Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque. </p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url(bahan/img/beauty-salon-albury-1.jpg);"></div>
		</div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Advanced nails art (gel polish)</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.</p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url(bahan/img/2-Aman-Luxury-Spa-Mayfair.jpg);"></div>
		</div>
		<div class="space-60"></div>
	</section>
	
	<section class="why-vanesha" style="background: url(design/img/background-kenapa-memilih-vanesha.jpg);">
		<div class="why-vanesha-layer">
			<h1>kenapa memilih vanesha ?</h2>
			<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.

			Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh.</p>
			<button class="btn btn-purple-white">baca selengkapnya</button>
		</div>
	</section>
	
	<section class="home-article">
		<h1 class="text-center">Top article</h1>
		<div class="slider1 home-article-content">
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		</div>
	</section>
	

	<section class="testimonial2" style="background: url(design/img/testimonial-background2.png);">
		<div class="testimonial2-layer">
			<h1>our <label>client</label> say</h1>
			<img src="design/img/testimonial-pager.png">
			<div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <div class="pic">
                        <img src="design/img/Hope Style.jpg" alt="">
                    </div>
                    <p class="description">
                        Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat.

						<br><br>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada.
                    </p>
                    <div class="testimonial-prof">
                        <span class="title">Imbar Winata</span>
                        <small>Web Developer</small>
                    </div>
                </div>
 
                <div class="testimonial">
                    <div class="pic">
                        <img src="design/img/tari.png" alt="">
                    </div>
                    <p class="description">
                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.
                    </p>
                    <div class="testimonial-prof">
                        <span class="title">Fuji Lestari</span>
                        <small>college student</small>
                    </div>
                </div>
            </div>
		</div>
	</section>
	
	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" /><script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
</style>
</body>
</html>