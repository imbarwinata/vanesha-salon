<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* Home */
if($_GET['page']=="home_banner"){
	include "home/home_banner_view.php";
}
elseif($_GET['page']=="home_content"){ 
	include "sample/crud_form_view.php"; 
}
/* Home - End */

/* Location */
elseif($_GET['page']=="location"){ 
	include "location/location_view.php"; 
}
elseif($_GET['page']=="location_add"){ 
	include "location/location_form_view.php"; 
}
elseif($_GET['page']=="location_edit"){ 
	include "location/location_form_view.php"; 
}
elseif($_GET['page']=="location_delete"){ 
	if($_GET['id']!=NULL){
		$where = ['LocationID'=>$_GET['id']];
		$delete = $db->delete("location",$where,"notlike");
		if($delete==true){ 
			$proccess->generateSuccessMessage("success","location",$_GET['id'],"success","Delete");
            echo "<script>window.location=('?page=location');</script>";
		}
		else{ 
			$proccess->generateSuccessMessage("failed","location",$_GET['id'],"failed","Delete");
			echo "<script>window.location=('?page=location');</script>";
		}
	}
	else{
		echo "<script>window.location=('?page=location');</script>";
	}
}
/* Location - end */

/* Article */
elseif($_GET['page']=="article"){ 
	include "article/article_view.php"; 
}
elseif($_GET['page']=="article_add"){ 
	include "article/article_form_view.php"; 
}
elseif($_GET['page']=="article_edit"){ 
	include "article/article_form_view.php";
}
elseif($_GET['page']=="article_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Show");
	header('Location: ?page=article');
}
elseif($_GET['page']=="article_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Hide");
	header('Location: ?page=article');
}
elseif($_GET['page']=="article_delete"){
	if($_GET['id']!=NULL){
		$id = $_GET['id'];
		$getData = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        unlink("../img/content/article/".$getData['ContentPointImage']);
			$proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Delete");
            header('Location: ?page=article');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","article",$_GET['id'],"failed","Delete");
			header('Location: ?page=article');
		}
	}
	else{
		header('Location: ?page=article');
	}
}
/* Article - end */

/* About */
elseif($_GET['page']=="about"){ 
	include "about/about_view.php"; 
}
elseif($_GET['page']=="about_add"){ 
	include "about/about_form_view.php"; 
}
elseif($_GET['page']=="about_edit"){ 
	include "about/about_form_view.php";
}
elseif($_GET['page']=="about_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Show");
	header('Location: ?page=about');
}
elseif($_GET['page']=="about_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Hide");
	header('Location: ?page=about');
}
elseif($_GET['page']=="about_delete"){
	if($_GET['id']!=NULL){
		$id = $_GET['id'];
		$getData = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        unlink("../img/content/about/".$getData['ContentPointImage']);
			$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Delete");
            header('Location: ?page=about');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","about",$_GET['id'],"failed","Delete");
			header('Location: ?page=about');
		}
	}
	else{
		header('Location: ?page=about');
	}
}
/* About - end */

/* FAQ */
elseif($_GET['page']=="faq"){ 
	include "faq/faq_view.php"; 
}
elseif($_GET['page']=="faq_add"){ 
	include "faq/faq_form_view.php"; 
}
elseif($_GET['page']=="faq_edit"){ 
	include "faq/faq_form_view.php";
}
elseif($_GET['page']=="faq_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Show");
	header('Location: ?page=faq');
}
elseif($_GET['page']=="faq_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Hide");
	header('Location: ?page=faq');
}
elseif($_GET['page']=="faq_delete"){
	if($_GET['id']!=NULL){
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        $proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Delete");
            header('Location: ?page=faq');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","faq",$_GET['id'],"failed","Delete");
			header('Location: ?page=faq');
		}
	}
	else{
		header('Location: ?page=faq');
	}
}
/* FAQ - end */

/* Social Media Link */
elseif($_GET['page']=="social_media_link"){ 
	include "social_media_link/social_media_link_view.php"; 
}
elseif($_GET['page']=="social_media_link_add"){ 
	include "social_media_link/social_media_link_form_view.php"; 
}
elseif($_GET['page']=="social_media_link_edit"){ 
	include "social_media_link/social_media_link_form_view.php";
}
elseif($_GET['page']=="social_media_link_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Show");
	header('Location: ?page=social_media_link');
}
elseif($_GET['page']=="social_media_link_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Hide");
	header('Location: ?page=social_media_link');
}
elseif($_GET['page']=="social_media_link_delete"){
	if($_GET['id']!=NULL){
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        $proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Delete");
            header('Location: ?page=social_media_link');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","social media link",$_GET['id'],"failed","Delete");
			header('Location: ?page=social_media_link');
		}
	}
	else{
		header('Location: ?page=social_media_link');
	}
}
/* Social Media Link - end */

elseif($_GET['page']=="logout"){ include "logout.php"; }
else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>