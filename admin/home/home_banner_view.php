<?php
if($_POST){
  if($_POST['title']==NULL OR $_POST['subtitle']==NULL){
    if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
    if($_POST['subtitle']==NULL){ $validation_subtitle = "The subtitle can not be empty."; }
    if($_POST['link']==NULL){ $validation_link = "The link can not be empty."; }
  }else{
    /* Update PROSES */
      $cek_data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Home Banner'","row");
      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
      $data['ContentID'] = $id;
      $data['ContentTitle'] = $validation->default_rules($_POST['title']);
      $data['ContentSubtitle'] = $validation->default_rules($_POST['subtitle']);
      $data['ContentLink'] = $validation->default_rules($_POST['link']);
      $data['ContentDate'] = $date->getCurrentDate();
      if($_FILES['banner']['name'] != NULL){
        $data['ContentImage']=$_FILES['banner']['name'];        
        unlink("../img/content/banner/".$cek_data['ContentImage']);
      }
      $where = ['ContentLabel'=> 'Home Banner'];
      $query_update = $db->update("content",$data,$where,"notlike");
      copy ($_FILES ["banner"]["tmp_name"], "../img/content/banner/".$data['ContentImage']);
      if($query_update==false){
        $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update Home Banner failed.";
        $_SESSION['success_parameter'] = "Update failed.";
        echo "<script>window.location=('?page=home_banner');</script>";
      }else{
        $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update Home Banner success.";
        $_SESSION['success_parameter'] = "Update success.";
        echo "<script>window.location=('?page=home_banner');</script>";
      }
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
  <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Banner
      </h1>
      <ol class="breadcrumb">
        <li><a href="?page=index"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banner</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
    <?php
        if(isset($_SESSION['success_message'])){
      ?>
          <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
          </div>
          <!-- Success Message - End -->
      <?php
          unset($_SESSION['success_type']);
          unset($_SESSION['success_message']);
          unset($_SESSION['success_parameter']);
        }
      ?>
          <div class="box">
          <?php
            $data = $db->query("SELECT * FROM content WHERE ContentLabel='Home Banner'");
            if($data==false){
                $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
                $data['ContentID'] = $id;
                $data['ContentLabel'] = "Home Banner";
                $data['ContentDate'] = $date->getCurrentDate();
                $query_insert = $db->insert("content",$data);
            }
            else{
              $data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Home Banner'","row");
              $page_flag = "Update";
            }
          ?>
            <div class="box-header">
              <h3 class="box-title" style="width:100%;">Banner</h3>
            </div>
            <form role="form" method="post" enctype="multipart/form-data" action="<?php $_SERVER["PHP_SELF"] ?>" style="width:94%; margin:0 auto;">
                <div class="box-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; }else{ echo $data['ContentTitle']; } ?>" class="form-control" placeholder="Title Banner ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label>Sub Title</label>
                    <input name="subtitle" type="text" value="<?php if($_POST['subtitle']!=NULL){ echo $_POST['subtitle']; }else{ echo $data['ContentSubTitle']; } ?>" class="form-control" placeholder="Sub Title Banner ..">
                <?php
                  if(isset($validation_subtitle)){ ?>
                    <em style="color:red;"><?php echo $validation_subtitle; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label>Link</label>
                    <input name="link" type="text" value="<?php if($_POST['link']!=NULL){ echo $_POST['link']; }else{ echo $data['ContentLink']; } ?>" class="form-control" placeholder="Link Banner ..">
                <?php
                  if(isset($validation_link)){ ?>
                    <em style="color:red;"><?php echo $validation_link; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                      <label id="label_image">Image Banner</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/banner/".$data['ContentImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                              <input type="file" class="default" name="banner" onchange="checkImage('banner')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>
                  <?php
                  if(isset($validation_banner)){ ?>
                    <em style="color:red;"><?php echo $validation_banner; ?></em>
                <?php
                  }
                ?>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=location');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
          </div>
        </section>
      </div>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>

  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.js"></script>
</body>
</html>