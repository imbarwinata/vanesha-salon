<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
  <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        F.A.Q
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-building"></i> <a href="?page=index"> &nbsp;F.A.Q</a></li>
    <?php
      if(preg_match("/add/", $_GET['page'])){ ?>
        <li class="active">Insert</li>
    <?php
      }
      else{ ?>
        <li class="active">Edit</li>
    <?php
      }
    ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->      
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <!-- general form elements -->
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("FAQ","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "FAQ";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title'])).$id;
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","faq",$id,"failed","Insert");
                  echo "<script>window.location=('?page=faq');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","faq",$id,"success","Insert");
                  echo "<script>window.location=('?page=faq');</script>";
                }
              }
            }
        ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Insert F.A.Q</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Question</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Question ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Answer</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em style="color:red;"><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=faq');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title']))."-".$_GET['id'];
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","faq",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=faq');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=faq');</script>";
                }
              }
            }
        ?>
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit F.A.Q</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Question</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Question ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Answer</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST){ if($_POST['description']!=NULL){ echo $_POST['description']; } else{echo "";} }else{ echo $data['ContentPointDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em style="color:red;"><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=faq');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php 

          }
        ?>
       </div>
      </section>
        <!-- /.box -->
      <!-- /.main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>
  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.js"></script>
<script src="../assets/plugins/ckeditor_full/ckeditor.js"  language="javascript" type="text/javascript"></script>
<script>
    var roxyFileman = '../assets/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
</body>
</html>
