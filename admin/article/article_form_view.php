<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
  <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Article
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-building"></i> <a href="?page=index"> &nbsp;Article</a></li>
    <?php
      if(preg_match("/add/", $_GET['page'])){ ?>
        <li class="active">Insert</li>
    <?php
      }
      else{ ?>
        <li class="active">Edit</li>
    <?php
      }
    ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->      
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <!-- general form elements -->
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "The subdescription can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("A","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "Article";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title'])).$id;
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                if($_FILES['image']['name'] != NULL){
                  $data['ContentPointImage'] = $_FILES['image']['name'];
                  copy($_FILES ["image"]["tmp_name"], "../img/content/article/".$data['ContentPointImage']);
                }
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","article",$_GET['id'],"failed","Insert");
                  echo "<script>window.location=('?page=article');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Insert");
                  echo "<script>window.location=('?page=article');</script>";
                }
              }
            }
        ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Insert Article</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Article ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Sub Description</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em style="color:red;"><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Description</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em style="color:red;"><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Image</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/article/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=article');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "The subdescription can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title']))."-".$_GET['id'];
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                if($_FILES['image']['name'] != NULL){
                  unlink("../img/content/article/".$cek_data['ContentPointImage']);
                  $data['ContentPointImage'] = $_FILES['image']['name'];
                }
                copy($_FILES ["image"]["tmp_name"], "../img/content/article/".$_FILES['image']['name']);
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","article",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=article');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=article');</script>";
                }
              }
            }
        ?>
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Article</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Article ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Sub Description</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST){ if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } else{echo "";} }else{ echo $data['ContentPointSubDescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em style="color:red;"><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Description</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST){ if($_POST['description']!=NULL){ echo $_POST['description']; } else{echo "";} }else{ echo $data['ContentPointDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em style="color:red;"><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Image</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/article/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=article');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php 

          }
        ?>
       </div>
      </section>
        <!-- /.box -->
      <!-- /.main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>
  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.js"></script>
<script src="../assets/plugins/ckeditor_full/ckeditor.js"  language="javascript" type="text/javascript"></script>
<script>
    var roxyFileman = '../assets/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
</body>
</html>
