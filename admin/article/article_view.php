<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Article
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-building"></i> &nbsp;Article</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">

          <!-- Success Message -->
      <?php
        if(isset($_SESSION['success_message'])){
      ?>
          <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
          </div>
          <!-- Success Message - End -->
      <?php
          unset($_SESSION['success_type']);
          unset($_SESSION['success_message']);
          unset($_SESSION['success_parameter']);
        }
      ?>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title" style="width:100%;">List Article 
                <a href="?page=<?php echo $_GET['page']; ?>_add" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span></a>
              </h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>SubDescription</th>
                    <th>Last Update</th>
                    <th>Show</th>
                    <th style="width:160px;">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel = 'Article' ORDER BY ContentPointDate DESC","result");
                  if($data!=NULL){
                    foreach ($data as $data_list):
                ?>
                    <tr>
                      <td><?= $data_list->ContentPointTitle; ?></td>
                      <td><?= $validation->truncateString($data_list->ContentPointSubDescription, 30, false); ?></td>
                      <td><?php echo $date->time_elapsed_string($data_list->ContentPointDate); ?></td>
                      <td>
                        <?php
                            if($data_list->ContentPointShow == 1){
                        ?>
                            <span class="label label-success">Show</span>
                        <?php
                            } else {
                        ?>
                            <span class="label label-danger">Hide</span>
                        <?php
                            }
                        ?>
                      </td>
                      <td>
                      <?php
                        if($data_list->ContentPointShow==0){ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_show&id=<?= $data_list->ContentPointID; ?>" class="btn btn-default btn-flat" data-toggle="tooltip" title="Show"><i class="fa fa-eye-slash"></i></a>
                      <?php
                        }else{ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_hide&id=<?= $data_list->ContentPointID; ?>" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Hide"><i class="fa fa-eye"></i></a>
                      <?php
                        }
                      ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_edit&id=<?= $data_list->ContentPointID; ?>" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                        <a onclick="if(confirm('Data will be deleted ?')){ window.location=('?page=<?php echo $_GET['page']; ?>_delete&id=<?= $data_list->ContentPointID; ?>') }" class="btn btn-danger btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                <?php 
                   endforeach;
                 }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>

  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<style type="text/css">
  .dataTables_filter {
     width: 50%;
     float: right;
     text-align: right;
  }
  .dataTables_paginate{
     width: 50%;
     float: right;
     text-align: right;
  }
</style>
<?php include('../templates/admin/js_view.php'); ?>
<?php include('../templates/admin/datatable_view.php'); ?>
</body>
</html>
