<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        CRUD
      </h1>
      <ol class="breadcrumb">
        <li><a href="?page=index"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">CRUD</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title" style="width:100%;">List CRUD 
                <a href="?page=<?php echo $_GET['page']; ?>_add" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span></a>
              </h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Field 1</th>
                    <th>Field 2</th>
                    <th>Field 3</th>
                    <th style="width:110px;">Action</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>Field 1 1 Value</td>
                      <td>Field 1 2 Value</td>
                      <td>Field 1 3 Value</td>
                      <td>
                        <a href="" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <tr>
                        <td>Field 2 1 Value</td>
                        <td>Field 2 2 Value</td>
                        <td>Field 2 3 Value</td>
                        <td>
                          <a href="" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                          <a href="" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>Field 3 1 Value</td>
                        <td>Field 3 2 Value</td>
                        <td>Field 3 3 Value</td>
                        <td>
                          <a href="" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                          <a href="" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
    </section>


  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>

  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<style type="text/css">
  .dataTables_filter {
     width: 50%;
     float: right;
     text-align: right;
  }
  .dataTables_paginate{
     width: 50%;
     float: right;
     text-align: right;
  }
</style>
<?php include('../templates/admin/js_view.php'); ?>
<?php include('../templates/admin/datatable_view.php'); ?>
</body>
</html>
