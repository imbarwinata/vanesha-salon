<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        CRUD
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="?page=index"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=index">CRUD</a></li>
        <li class="active">Insert</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->      
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">CRUD</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input name="title" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Sub Title</label>
                    <input name="subtitle" type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <input name="image" type="file" id="exampleInputFile">
                    <p class="help-block">Example block-level help text here.</p>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Description</label>
                    <textarea name="description" class="form-control" id="ckeditor1"></textarea>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Check me out
                    </label>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
       </div>
      </section>
        <!-- /.box -->
      <!-- /.main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>
  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/ckeditor_full/ckeditor.js"  language="javascript" type="text/javascript"></script>
<script>
    var roxyFileman = '../assets/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
</body>
</html>
