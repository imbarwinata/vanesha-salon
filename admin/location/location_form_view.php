<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Location
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-building"></i> <a href="?page=index"> &nbsp;Location</a></li>
    <?php
      if(preg_match("/add/", $_GET['page'])){ ?>
        <li class="active">Insert</li>
    <?php
      }
      else{ ?>
        <li class="active">Edit</li>
    <?php
      }
    ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->      
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <!-- general form elements -->
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['pos']==NULL){ $validation_pos = "The kode pos can not be empty."; }
                if($_POST['fare']==NULL){ $validation_fare = "The fare fare can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("L","ymd","location","LocationID",3);
                $data['LocationID'] = $id;
                $data['LocationTitle'] = $validation->default_rules($_POST['title']);
                $data['LocationKodePos'] = $validation->default_rules($_POST['pos']);
                $data['LocationFare'] = $validation->default_rules($_POST['fare']);
                $data['LocationShow'] = $validation->default_rules($_POST['show']);
                $data['LocationEntryDate'] = $date->getCurrentDate();
                $data['LocationEntryName'] = 'Admin';
                $data['LocationUpdateName'] = 'Admin';
                $data['LocationUpdateDate'] = $date->getCurrentDate();
                $query_insert = $db->insert("location",$data);
                if($query_insert==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert location $id failed.";
                  $_SESSION['success_parameter'] = "Insert failed.";
                  echo "<script>window.location=('?page=location');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert location $id success.";
                  $_SESSION['success_parameter'] = "Insert success.";
                  echo "<script>window.location=('?page=location');</script>";
                }
              }
            }
        ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Insert Location</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Location ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos</label>
                    <input name="pos" type="text" value="<?php if($_POST['pos']!=NULL){ echo $_POST['pos']; } ?>" class="form-control" placeholder="Post Kode ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em style="color:red;"><?php echo $validation_pos; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Fare</label>
                    <div class="input-group">
                      <label class="input-group-addon">Rp. </label>
                      <input name="fare" type="text" value="<?php if($_POST['fare']!=NULL){ echo $_POST['fare']; } ?>" class="form-control" placeholder="Fare ..">
                    </div>
                <?php
                  if(isset($validation_pos)){ ?>
                    <em style="color:red;"><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=location');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $data = $db->query("SELECT * FROM location WHERE LocationID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['pos']==NULL){ $validation_pos = "The kode pos can not be empty."; }
                if($_POST['fare']==NULL){ $validation_fare = "The kode fare can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $data['LocationTitle'] = $validation->default_rules($_POST['title']);
                $data['LocationKodePos'] = $validation->default_rules($_POST['pos']);
                $data['LocationFare'] = $validation->default_rules($_POST['fare']);
                $data['LocationShow'] = $validation->default_rules($_POST['show']);
                $data['AdminID'] = $_SESSION['AdminID'];
                $data['LocationDate'] = $date->getCurrentDate();
                $where = ['LocationID'=>$_GET['id']];
                $query_update = $db->update("location",$data,$where,"notlike");
                if($query_update==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update location ".$_GET['id']." failed.";
                  $_SESSION['success_parameter'] = "Update failed.";
                  echo "<script>window.location=('?page=location');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update location ".$_GET['id']." success.";
                  $_SESSION['success_parameter'] = "Update success.";
                  echo "<script>window.location=('?page=location');</script>";
                }
              }
            }
        ?>
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Location</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['LocationTitle']; } ?>" class="form-control" placeholder="Location ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos</label>
                    <input name="pos" type="text" value="<?php if($_POST){ if($_POST['pos']!=NULL){ echo $_POST['pos']; } else{echo "";} }else{ echo $data['LocationKodePos']; } ?>" class="form-control" placeholder="Post Kode ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em style="color:red;"><?php echo $validation_pos; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Fare</label>
                    <input name="fare" type="number" step="1000" value="<?php if($_POST){ if($_POST['fare']!=NULL){ echo $_POST['fare']; } else{echo "";} }else{ echo $data['LocationFare']; } ?>" class="form-control" placeholder="Fare ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em style="color:red;"><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['LocationShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['LocationShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=location');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php 

          }
        ?>
       </div>
      </section>
        <!-- /.box -->
      <!-- /.main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>
  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/ckeditor_full/ckeditor.js"  language="javascript" type="text/javascript"></script>
<script>
    var roxyFileman = '../assets/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
</body>
</html>
