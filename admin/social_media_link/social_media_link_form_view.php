<!DOCTYPE html>
<html>
<head>
  <?php include('../templates/admin/head_view.php'); ?>
  <?php include('../templates/admin/css_view.php'); ?>
  <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.css">
  <link rel="stylesheet" type="text/css" href="../assets/plugins/fontawesome-picker/css/fontawesome-iconpicker.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include('../templates/admin/header_view.php'); ?>
  <?php include('../templates/admin/navbar_view.php'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Social Media Link
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-link"></i> <a href="?page=index"> &nbsp;Social Media Link</a></li>
    <?php
      if(preg_match("/add/", $_GET['page'])){ ?>
        <li class="active">Insert</li>
    <?php
      }
      else{ ?>
        <li class="active">Edit</li>
    <?php
      }
    ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->      
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <!-- general form elements -->
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['link']==NULL){ $validation_link = "The link can not be empty."; }
                if($_POST['icon']==NULL){ $validation_icon = "The icon can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("SML","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "Social Media Link";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['link']);
                $data['ContentPointIcon'] = $validation->wysiwyg($_POST['icon']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","Social Media Link",$id,"failed","Insert");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","Social Media Link",$id,"success","Insert");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }
              }
            }
        ?>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Insert Social Media Link</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Social Media Name</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Social Media Name..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Link</label>
                    <input name="link" type="url" value="<?php if($_POST['link']!=NULL){ echo $_POST['link']; } ?>" class="form-control" placeholder="Social Media Name..">
                <?php
                  if(isset($validation_link)){ ?>
                    <em style="color:red;"><?php echo $validation_link; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <input type="text" name="icon" class="form-control icp icp-auto icon_picker" value="<?php if($_POST['icon']!=NULL){ echo $_POST['icon']; } ?>">
                <?php
                  if(isset($validation_icon)){ ?>
                    <em style="color:red;"><?php echo $validation_icon; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=social_media_link');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['link']==NULL){ $validation_link = "The link can not be empty."; }
                if($_POST['icon']==NULL){ $validation_icon = "The icon can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['link']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['icon']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","Social Media Link",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","Social Media Link",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }
              }
            }
        ?>
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Social Media Link</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Social Media Name</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Link Social Media ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em style="color:red;"><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Link</label>
                    <input name="link" type="url" value="<?php if($_POST){ if($_POST['link']!=NULL){ echo $_POST['link']; } else{echo "";} }else{ echo $data['ContentPointSubDescription']; } ?>" class="form-control" placeholder="Link Social Media ..">
                <?php
                  if(isset($validation_link)){ ?>
                    <em style="color:red;"><?php echo $validation_link; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <input type="text" name="icon" class="form-control icp icp-auto icon_picker" value="<?php if($_POST){ if($_POST['icon']!=NULL){ echo $_POST['icon']; } else{echo "";} }else{ echo $data['ContentPointIcon']; } ?>">
                <?php
                  if(isset($validation_icon)){ ?>
                    <em style="color:red;"><?php echo $validation_icon; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em style="color:red;"><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=social_media_link');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            </div>
        <?php 

          }
        ?>
       </div>
      </section>
        <!-- /.box -->
      <!-- /.main content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('../templates/admin/footer_view.php'); ?>
  <!-- Control Sidebar -->
  <?php include('../templates/admin/control_sidebar_view.php'); ?>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php include('../templates/admin/js_view.php'); ?>
<script src="../assets/plugins/fontawesome-picker/js/fontawesome-iconpicker.min.js"></script>
<script type="text/javascript">
    $(".icon_picker").iconpicker();
</script>
<script>
    var roxyFileman = '../assets/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
</body>
</html>
