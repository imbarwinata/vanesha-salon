<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../img/default/admin2.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$_SESSION['AdminName'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="page" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview <?php if(preg_match("/home/", $_GET['page'])){ echo "active";} ?>">
          <a href="#">
            <i class="fa fa-home"></i>
            <span>Home</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if(preg_match("/home_banner/", $_GET['page'])){ echo "active";} ?>"><a href="?page=home_banner"><i class="fa fa-circle-o"></i> Banner</a></li>
            <li class="<?php if(preg_match("/home_content/", $_GET['page'])){ echo "active";} ?>"><a href="?page=home_content"><i class="fa fa-circle-o"></i> Content</a></li>
          </ul>
        </li>
        <li class="<?php if(preg_match("/location/", $_GET['page'])){ echo "active";} ?>">
          <a href="?page=location">
            <i class="fa fa-building"></i> <span>Location</span>
          </a>
        </li>
        <li class="treeview <?php if(preg_match("/product/", $_GET['page'])){ echo "active";} ?>">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Services</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="?page=category"><i class="fa fa-circle-o"></i> Category</a></li>
            <li><a href="?page=services"><i class="fa fa-circle-o"></i> Services</a></li>
          </ul>
        </li>
        <li class="<?php if(preg_match("/about/", $_GET['page'])){ echo "active";} ?>">
          <a href="?page=about">
            <i class="fa fa-users"></i>
            <span>About</span>
          </a>
        </li>
        <li class="<?php if(preg_match("/article/", $_GET['page'])){ echo "active";} ?>">
          <a href="?page=article">
            <i class="fa fa-newspaper-o"></i>
            <span>Article</span>
          </a>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-envelope"></i> <span>Contact Message</span>
            <small class="label pull-right bg-green">new</small>
          </a>
        </li>
        <li class="<?php if(preg_match("/faq/", $_GET['page'])){ echo "active";} ?>">
          <a href="?page=faq">
            <i class="fa fa-question"></i>
            <span>FAQ</span>
          </a>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-envelope"></i> <span>Subscribe</span>
            <small class="label pull-right bg-green">new</small>
          </a>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-envelope"></i> <span>Transaction</span>
            <small class="label pull-right bg-green">new</small>
          </a>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-envelope"></i> <span>Testimony</span>
            <small class="label pull-right bg-green">new</small>
          </a>
        </li>
        <li class="<?php if(preg_match("/social_media_link/", $_GET['page'])){ echo "active";} ?>">
          <a href="?page=social_media_link">
            <i class="fa fa-link"></i>
            <span>Social Media Link</span>
          </a>
        </li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Privacy and Policy</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Terms and Condition</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>