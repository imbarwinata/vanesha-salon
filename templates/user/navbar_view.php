<nav class="navbar navbar-default navbar-fixed-top" id="navbar" role="navigation bg-white" style="border-bottom: none;">
  <div style="margin: 0 auto;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button style="border:1px solid rgba(128, 67, 123,0.3);" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
		<i class="fa fa-align-justify" style="color:rgba(128, 67, 123,1.0);"></i>
      </button>
      <a class="navbar-brand" href="#"><img style="height:150%;" src="design/img/logo.png" alt=""></a>
    </div>

   	<div id="logo-primary" class="text-center" style="" onClick="window.location=('index.php')">
    	<img src="design/img/logo.png" alt="">
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse bg-white navbar-primary element-center" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!--<li class="pull-left"><a href="#">Dashboard</a></li>-->
		<li><a class="navbar-link" href="index.php?link=aboutus"><span>About Us</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan&category=facial"><span>Facial</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan&category=waxing"><span>Waxing</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan&category=hair"><span>Hair</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan&category=nails"><span>Nails</span></a></li>
		<li><a class="navbar-link" href="index.php?link=article"><span>Article</span></a></li>
		<li><a class="navbar-link" href="index.php?link=contactus"><span>Contact Us</span></a></li>
		<li><a class="navbar-link" href="index.php?link=account"><span>Account</span></a></li>
		<li class="social pull-right"><a href="#" id="search" onClick="toggleSearch()"><i class="fa fa-search"></i></a></li>
      </ul>
	  <form class="form-search pull-right" style="border-bottom: none;" method="get">	
  			<div class="input-group col-lg-3 pull-right search-area">
				<span class="input-group-addon" id="basic-addon">
					<button type="submit"><i class="fa fa-search"></i></button>
				</span>
			  <input type="text" class="form-control" placeholder="Cari disini ..." aria-describedby="basic-addon1">
			</div>
	  </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>