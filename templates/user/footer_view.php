<section class="container pre-footer" style="padding: 25px 0px 50px 0px;">
		<div class="col-sm-3 footer-part">
			<h3>Services</h3>
			<ul>
				<li><a href="#">facial</a></li>
				<li><a href="#">waxing</a></li>
				<li><a href="#">hairs</a></li>
				<li><a href="#">nails</a></li>
				<li><a href="#">article</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>member</h3>
			<ul>
				<li><a href="#">sign up</a></li>
				<li><a href="#">login</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>company</h3>
			<ul>
				<li><a href="?link=about_us">about us</a></li>
				<li><a href="?link=contact_us">contact us</a></li>
				<li><a href="?link=faq">F.A.Q</a></li>
				<li><a href="?link=terms_and_condition">terms and condition</a></li>
				<li><a href="?link=privacy_policy">privacy policy</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>follow us</h3>
			<div class="social-media" style="margin-top: 20px;">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-pinterest"></i></a>
				<a href="#"><i class="fa fa-youtube"></i></a>
			</div>
			
			<h3 style="margin-top: 40px;">subscribe us</h3>
			<p class="footer-subscribe">Dapatkan notifikasi ketika kami merilis layanan baru.</p>
			<div class="input-group">
			  <input type="text" class="form-control" placeholder="Masukkan email anda ..." aria-describedby="basic-addon1">
				<span class="input-group-addon" id="basic-addon">
					<button type="button"><i class="fa fa-send"></i></button>
				</span>
			</div>
		</div>
	</section>
	<section class="container-fluid footer">
		<div class="container center-block">
			<label class="footer-content text-center">Copyright 2017 Vanesha Salon. All Right Reserved.</label>
		</div>
	</section>