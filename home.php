<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title>
<link href="design/plugins/bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"></link>
<link href="design/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"></link>
<link rel="stylesheet" href="design/less/styles.css">
<link rel="stylesheet" href="design/less/styles.less">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<!--<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Kalam">-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" id="navbar" role="navigation bg-white" style="border-bottom: none;">
  <div style="margin: 0 auto;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button style="border:1px solid rgba(128, 67, 123,0.3);" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
		<i class="fa fa-align-justify" style="color:rgba(128, 67, 123,1.0);"></i>
      </button>
      <a class="navbar-brand" href="#"><img style="height:150%;" src="design/img/logo.png" alt=""></a>
    </div>

   	<div id="logo-primary" class="text-center" style="" onClick="window.location=('#')">
    	<img src="design/img/logo.png" alt="">
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse bg-white navbar-primary element-center" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!--<li class="pull-left"><a href="#">Dashboard</a></li>-->
		<li><a class="navbar-link" href="index.php?link=aboutus"><span>About Us</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan"><span>Facial</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan"><span>Waxing</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan"><span>Hair</span></a></li>
		<li><a class="navbar-link" href="index.php?link=layanan"><span>Nails</span></a></li>
		<li><a class="navbar-link" href="index.php?link=article"><span>Article</span></a></li>
		<li><a class="navbar-link" href="index.php?link=contactus"><span>Contact Us</span></a></li>
		<li><a class="navbar-link" href="index.php?link=account"><span>Account</span></a></li>
		<li class="social pull-right"><a href="#" id="search" onClick="toggleSearch()"><i class="fa fa-search"></i></a></li>
      </ul>
	  <form class="form-search pull-right" style="border-bottom: none;" method="get">	
  			<div class="input-group col-lg-3 pull-right search-area">
				<span class="input-group-addon" id="basic-addon">
					<button type="submit"><i class="fa fa-search"></i></button>
				</span>
			  <input type="text" class="form-control" placeholder="Cari disini ..." aria-describedby="basic-addon1">
			</div>
	  </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="body">
	<section class="banner" id="image" style="background-image: url(design/img/background-banner2.jpg);">
		<div id="banner2"></div>
		<section class="banner-content">
			<h2>Mau tau tips kecantikan ?</h2>
			<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<button>Kunjungi</button>
		</section>
	</section>
	
	<section class="home-service">
		<h2 class="text-center">Top services</h2>
		<div class="space-15"></div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Cream bath and hair dry</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.

				<br><br>Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque. </p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url('bahan/img/Fotolia_63073892_Subscription_Monthly_M1.jpeg');"></div>
		</div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Facial by glowy</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.

				<br><br>Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque. </p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url(bahan/img/beauty-salon-albury-1.jpg);"></div>
		</div>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3>Advanced nails art (gel polish)</h3>
				<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.</p>
				<button class="btn btn-white-purple">Pesan sekarang</button>
			</div>
			<div class="home-services-background" style="background-image:url(bahan/img/2-Aman-Luxury-Spa-Mayfair.jpg);"></div>
		</div>
		<div class="space-60"></div>
	</section>
	
	<section class="why-vanesha" style="background: url(design/img/background-kenapa-memilih-vanesha.jpg);">
		<div class="why-vanesha-layer">
			<h1>kenapa memilih vanesha ?</h2>
			<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.

			Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh.</p>
			<button class="btn btn-purple-white">baca selengkapnya</button>
		</div>
	</section>
	
	<section class="home-article">
		<h1 class="text-center">Top article</h1>
		<div class="slider1 home-article-content">
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		  <div class="slide">
			  <div class="slide-article-background" style="background: url(bahan/img/beauty-salon-albury-1.jpg)"></div>
			  <h3>Lorem ipsum</h3>
			  <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.</p>
		  </div>
		</div>
	</section>
	

	<section class="testimonial2" style="background: url(design/img/testimonial-background2.png);">
		<div class="testimonial2-layer">
			<h1>our <label>client</label> say</h1>
			<img src="design/img/testimonial-pager.png">
			<div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <div class="pic">
                        <img src="design/img/Hope Style.jpg" alt="">
                    </div>
                    <p class="description">
                        Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat.

						<br><br>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada.
                    </p>
                    <div class="testimonial-prof">
                        <span class="title">Imbar Winata</span>
                        <small>Web Developer</small>
                    </div>
                </div>
 
                <div class="testimonial">
                    <div class="pic">
                        <img src="design/img/tari.png" alt="">
                    </div>
                    <p class="description">
                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.
                    </p>
                    <div class="testimonial-prof">
                        <span class="title">Fuji Lestari</span>
                        <small>college student</small>
                    </div>
                </div>
            </div>
		</div>
	</section>
	
	<section class="container pre-footer" style="padding: 25px 0px 50px 0px;">
		<div class="col-sm-3 footer-part">
			<h3>Services</h3>
			<ul>
				<li><a href="#">facial</a></li>
				<li><a href="#">waxing</a></li>
				<li><a href="#">hairs</a></li>
				<li><a href="#">nails</a></li>
				<li><a href="#">article</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>member</h3>
			<ul>
				<li><a href="#">sign up</a></li>
				<li><a href="#">login</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>company</h3>
			<ul>
				<li><a href="#">about us</a></li>
				<li><a href="#">contact us</a></li>
				<li><a href="#">F.A.Q</a></li>
				<li><a href="#">terms and condition</a></li>
				<li><a href="#">privacy policy</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>follow us</h3>
			<div class="social-media" style="margin-top: 20px;">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-pinterest"></i></a>
				<a href="#"><i class="fa fa-youtube"></i></a>
			</div>
			
			<h3 style="margin-top: 40px;">subscribe us</h3>
			<p class="footer-subscribe">Dapatkan notifikasi ketika kami merilis layanan baru.</p>
			<div class="input-group">
			  <input type="text" class="form-control" placeholder="Masukkan email anda ..." aria-describedby="basic-addon1">
				<span class="input-group-addon" id="basic-addon">
					<button type="button"><i class="fa fa-send"></i></button>
				</span>
			</div>
		</div>
	</section>
	<section class="container-fluid footer">
		<div class="container center-block">
			<label class="footer-content text-center">Copyright 2017 Vanesha Salon. All Right Reserved.</label>
		</div>
	</section>
</div>

<script src="design/js/jquery-3.1.1.min.js"></script>
<script src="design/plugins/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<script src="design/js/custom.js"></script>
<script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {
	left: -45px;
}

.bx-wrapper .bx-next {
	right: -35px;
}
</style>
</body>
</html>