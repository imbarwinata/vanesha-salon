-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2017 at 01:51 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaneshasalon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AdminID` int(11) NOT NULL,
  `AdminUserName` varchar(20) NOT NULL,
  `AdminPassword` varchar(50) NOT NULL,
  `AdminName` varchar(50) NOT NULL,
  `AdminEmail` varchar(50) NOT NULL,
  `AdminEntryName` varchar(50) NOT NULL,
  `AdminEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AdminID`, `AdminUserName`, `AdminPassword`, `AdminName`, `AdminEmail`, `AdminEntryName`, `AdminEntryDate`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'administrator@gmail.com', '-', '2017-02-15 17:16:07'),
(2, 'imbarwinata', 'adec79fd87d04960046628531c4a56b714d160d8', 'Imbar Winata', 'imbarwinata@gmail.com', 'admin', '2017-02-15 18:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `ContentID` varchar(20) NOT NULL,
  `ContentLabel` varchar(30) NOT NULL,
  `ContentTitle` varchar(30) NOT NULL,
  `ContentSubTitle` varchar(50) NOT NULL,
  `ContentDescription` text NOT NULL,
  `ContentEntryName` varchar(50) NOT NULL,
  `ContentEntryDate` date NOT NULL,
  `ContentUpdateName` varchar(50) NOT NULL,
  `ContentUpdateDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `LocationID` varchar(20) NOT NULL,
  `LocationTitle` varchar(30) NOT NULL,
  `LocationKodePos` varchar(5) NOT NULL,
  `LocationFare` int(11) NOT NULL,
  `LocationShow` enum('0','1') NOT NULL,
  `LocationEntryName` varchar(50) NOT NULL,
  `LocationEntryDate` datetime NOT NULL,
  `LocationUpdateName` varchar(50) NOT NULL,
  `LocationUpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`LocationID`, `LocationTitle`, `LocationKodePos`, `LocationFare`, `LocationShow`, `LocationEntryName`, `LocationEntryDate`, `LocationUpdateName`, `LocationUpdateDate`) VALUES
('L170215001', 'Cibinong', '16911', 8000, '1', 'Admin', '2017-02-15 13:53:56', 'Admin', '2017-02-16 23:13:07'),
('L170215002', 'Tamansari', '16610', 0, '1', 'Admin', '2017-02-15 15:15:39', 'Admin', '2017-02-15 15:47:43'),
('L170215003', 'Babakan Madang', '16810', 10000, '1', 'Admin', '2017-02-15 15:16:00', 'Admin', '2017-02-15 18:29:46'),
('L170215004', 'Cikoan', '16810', 0, '1', 'Admin', '2017-02-15 15:55:14', 'Admin', '2017-02-15 15:55:14'),
('L170215005', 'Ciomas', '16610', 0, '1', 'Admin', '2017-02-15 15:55:35', 'Admin', '2017-02-15 15:55:35'),
('L170215006', 'Gunung Sindur', '16340', 0, '1', 'Admin', '2017-02-15 15:55:55', 'Admin', '2017-02-15 15:55:55'),
('L170215007', 'Leuwisadeng', '16640', 0, '1', 'Admin', '2017-02-15 15:56:36', 'Admin', '2017-02-15 15:56:36'),
('L170215008', 'Ranca Bungur', '16310', 0, '1', 'Admin', '2017-02-15 15:57:03', 'Admin', '2017-02-15 15:57:03'),
('L170215009', 'Pamijahan', '16810', 0, '1', 'Admin', '2017-02-15 15:57:15', 'Admin', '2017-02-15 15:57:15'),
('L170215010', 'Bojonggede', '16920', 12000, '1', 'Admin', '2017-02-15 15:57:41', 'Admin', '2017-02-16 23:12:33'),
('L170215011', 'Cibungbulang', '16630', 0, '1', 'Admin', '2017-02-15 15:58:16', 'Admin', '2017-02-15 15:58:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`ContentID`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`LocationID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AdminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
